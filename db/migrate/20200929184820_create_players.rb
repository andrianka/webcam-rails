class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.string  :name
      t.date    :date_of_birth
      t.string  :place_of_birth
      t.integer :age
      t.float   :height
      t.string  :position
      t.string  :foot
      t.string  :current_club
      t.timestamps
    end
  end
end
