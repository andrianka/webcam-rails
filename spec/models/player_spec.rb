RSpec.describe Player, type: :model do
  let(:subject) { create(:player) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:foot) }
    it { should validate_numericality_of(:age).only_integer }
    it { should validate_inclusion_of(:foot).in_array(%w[Left Right Both]) }
    it { is_expected.to have_one_attached(:picture) }
  end
end
