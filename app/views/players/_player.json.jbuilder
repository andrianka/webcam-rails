# frozen_string_literal: true

json.extract! object :id, *attributes: [:id, :date_of_birth, :place_of_birth, :age, :height, :position, :foot, :current_club, :created_at, :updated_at]

json.url player_url(player, format: :json)
