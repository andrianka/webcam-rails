document.addEventListener('turbolinks:load', function(){
  if(document.getElementById("snap")){
    var cameraClick         = new Audio();
    var changePictureButton = document.getElementById('changePicture');

    if(document.getElementById('snap') && document.getElementById('viewfinder')){
      var snapButton = document.getElementById('snap');
      var videoDiv   = document.getElementById('viewfinder');

      videoDiv.style.display   = 'none';
      snapButton.style.display = 'none';
    }

    function hasNavigator(){
      return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)
    }

    if(hasNavigator()){
      var canvas = document.createElement('canvas');
      var video  = document.querySelector('video');

      var hiddenPlayerPicture = document.getElementById('player_player_picture');

      var constraints = {
        video: {
          width: { min: 288, ideal: 288 },
          heigth: { min: 301, ideal: 301 }
        }
      };

      if (video) {
        navigator.mediaDevices.getUserMedia(constraints)
          .then(function(stream){ video.srcObject = stream })
      }

      if (changePictureButton) {
        changePictureButton.onclick = function(){
          videoDiv.style.display = '';
          snapButton.style.display = '';
        }
      }

      if (snapButton) {
        snapButton.onclick = function () {
          cameraClick.play();

          canvas.width  = video.width;
          canvas.height = video.height;

          canvas.getContext('2d').drawImage(video, 0, 0);

          var dataUrl = canvas.toDataURL('image/jpeg');

          document.getElementById('shot').src = dataUrl;

          hiddenPlayerPicture.value = dataUrl;

          videoDiv.style.display   = 'none';
          snapButton.style.display = 'none';
        }
      }
    }
  }
});
