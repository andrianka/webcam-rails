# frozen_string_literal: true

RSpec.describe 'Players', type: :request do
  let(:player) { create(:player) }

  describe 'GET#index' do
    subject { get players_path }
    it 'returns http success' do
      subject
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET#show' do
    context 'success' do
      subject { get player_path(player.id) }
      it 'returns http success' do
        subject
        expect(response).to have_http_status(:success)
      end
    end

    # context 'failing' do
    #   subject { get player_path('non-existing-id') }
    #
    #   it 'returns http not_found with non-existing id' do
    #     subject
    #     expect(response).to have_http_status(:not_found)
    #   end
    # end
  end
  describe 'GET#edit' do
    subject { get edit_player_path(player.id) }
    it 'returns http success' do
      subject
      expect(response).to have_http_status(:success)
    end
  end
  describe 'POST#create' do
    let(:params) { attributes_for(:player) }
    subject { post players_path, params: { players: params } }
    it 'returns http success' do
      subject
      expect(response).to have_http_status(:success)
    end
    # TODO: fix this test
    # context 'failing' do
    #   subject { post players_path, params: { players: {} } }
    #   it 'returns http bad request' do
    #     subject
    #     expect(response).to have_http_status(:bad_request)
    #   end
    # end
  end
end
