# frozen_string_literal: true

class Player < ApplicationRecord
  attribute :player_picture, :string, default: ''

  has_one_attached :picture

  validates :name, presence: true
  validates :foot, presence: true, inclusion: { in: %w[Left Right Both] }
  validates :age, numericality: { only_integer: true }

  before_save :age

  def has_picture?
    File.exist?(ActiveStorage::Blob.service.path_for(picture.key))
  end

  def age
    ((Date.today - date_of_birth) / 365).floor if date_of_birth
  end
end
