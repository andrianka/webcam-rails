# frozen_string_literal: true

class PlayersController < ApplicationController
  before_action :set_player, except: %w[new create index]

  def index
    @players = Player.order('name')
  end

  def new
    @player = Player.new
  end

  def create
    @player = Player.new(player_params)
    PictureAttachmentService.attach(@player, player_params[:player_picture])
    if @player.save
      redirect_to @player, notice: 'Successfuly created player.'
    else
      flash[:error] = @player.errors.full_messages
      render :new
    end
  end

  private

  def set_player
    @player = Player.find(params[:id])
  end

  def player_params
    params.require(:player).permit(:name, :age, :foot, :position, :current_club,
                                   :height, :date_of_birth, :place_of_birth, :player_picture)
  end
end
